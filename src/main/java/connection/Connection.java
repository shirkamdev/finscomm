package connection;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import configuration.Configuration;
import configuration.Logger;
import connection.exeptions.CouldNotAwaitForResponseException;
import connection.exeptions.NoPLCResponseException;
import connection.exeptions.PLCConnectErrorException;
import connection.exeptions.PLCConnectResponseErrorException;
import connection.exeptions.PLCNotConnectedException;
import connection.exeptions.PLCRefusesConnectionException;
import models.FINS.FINSCode;
import models.FINS.FINSConstants;
import models.FINS.FINSMessage;

/**
 * Allows connecting to a PLC and sending and receiving FINS messages.
 */
public class Connection {
	/**
	 * IP of the PLC which we are connecting.
	 */
	private String ip;
	/**
	 * Port of the PLC where we are connecting.
	 */
	private int port;
	
	/**
	 * Socket that holds the connection between the PLC and the local
	 * device.
	 */
	private Socket plcConnection;
	/**
	 * If we are connected
	 */
	private boolean connected;
	
	/**
	 * Stream that sends data to PLC
	 */
	private DataOutputStream streamToServer;
	/**
	 * Stream that receives data from PLC
	 */
    private InputStream streamFromServer;
    
    /**
     * Timeout while trying to connect, in milliseconds
     */
    private int timeout;
	
    /**
     * Setups a connection but does not connect to the specified address
     * @param ip The IP to use on the connection
     * @param port The port to use on the connection
     */
	public Connection(String ip, int port) {
		this.ip = ip;
		this.port = port;
		
		connected = false;
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * If we are connected to the PLC. To do so, you must call the {@link Connection#connect() 
	 * connect()} method.
	 * @return
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * Tries to connect to a PLC with the given address and port.
	 * @throws PLCConnectErrorException If is not possible to connect to the PLC
	 * @throws PLCConnectResponseErrorException The PLC response is not valid
	 * @throws PLCRefusesConnectionException The PLC refuses connection
	 * @throws NoPLCResponseException There is no response from the PLC
	 */
	public void connect() throws PLCConnectErrorException, 
	PLCConnectResponseErrorException, 
	PLCRefusesConnectionException, NoPLCResponseException {
		// If we are connected, there is nothing to do.
		if(connected)
			return;
		
		Logger.Debug("Trying to connecto to PLC at "+this.ip+":"+this.port+" ...");
		// Initialize the socket
		SocketAddress address = new InetSocketAddress(this.ip, this.port);
        plcConnection = new Socket();
        
        // Try to establish a connection
        try {
        	plcConnection.connect(address, timeout);
            streamToServer = new DataOutputStream(plcConnection.getOutputStream());
            streamFromServer = plcConnection.getInputStream();
        } catch (IOException e) {
        	try {
				plcConnection.close();
			} catch (IOException e1) {
				// TODO Ver que hacemos con esta excepcion
			}
            throw new PLCConnectErrorException(this.ip, this.port);
        }
        
        Logger.Debug("Sending connect FINS message to PLC at "+this.ip+":"+this.port);
        // Send a FINS connect message
        FINSMessage connectMessage = new FINSMessage(FINSCode.CONNECT);
        try {
        	Logger.Debug(connectMessage.getMessageBytes());
            streamToServer.write(connectMessage.getMessageBytes());
        } catch (IOException e) {
        	try {
        		// Better safe than sorry, check if the connection is closed before closing it
	        	if(!plcConnection.isClosed())
	        		plcConnection.close();
        	} catch(IOException ex) {
        		// TODO Ver que hacemos con esta excepcion
        	}
        	throw new PLCRefusesConnectionException(this.ip, this.port);
        }
        
        // Wait until response
        waitResponse();
        
        Logger.Debug("Trying to read response of PLC at "+this.ip+":"+this.port);
        // Read the response
        byte[] inputBytes = new byte[32];
        int bytesRead = -1;
        try {
            if (streamFromServer.available() > 0) {
            	Logger.Debug("There is a response from "+this.ip+":"+this.port);
                bytesRead = streamFromServer.read(inputBytes);
            }
            if (bytesRead > 0) {
                Logger.Debug(inputBytes);
                // TODO Read response
            }
        } catch (IOException e) {
            throw new PLCConnectResponseErrorException(ip, port);
        }
        
        this.connected = true;
	}
	
	/**
	 * Disconnects from PLC.
	 */
	public void disconnect() {		
		if(this.connected) {
			Logger.Debug("Disconnecting from "+this.ip+":"+this.port);
			if(!this.plcConnection.isClosed()) {
				try {
					streamFromServer.close();
					streamToServer.close();
					plcConnection.close();
				} catch(IOException ex) {
					// TODO Launch exeption?
				}
				
				streamFromServer = null;
				streamToServer = null;
				plcConnection = null;
				connected = false;
			}
		} else {
			Logger.Debug("PLC at "+this.ip+":"+this.port+" not connected.");
		}
	}
	
	/**
	 * Sends a {@link FINSMessage} to the PLC.
	 * @param message The message to send
	 * @throws PLCNotConnectedException If there is no connection to a PLC.
	 * @throws PLCRefusesConnectionException The PLC refused the connection. 
	 * @throws PLCConnectResponseErrorException The PLC response is not valid.
	 * @throws PLCConnectErrorException There was an error when trying to connect to the PLC.
	 * @throws NoPLCResponseException There is no response from the PLC.
	 */
	public int[] sendMessageToPLC(FINSMessage message) 
			throws PLCNotConnectedException, PLCConnectErrorException, 
			PLCConnectResponseErrorException, PLCRefusesConnectionException, 
			NoPLCResponseException {
		if(message == null)
			throw new IllegalArgumentException("Error: You cannot send a NULL "
					+ "message");
		
		if(!this.connected)
			connect();
		
		if(!plcConnection.isConnected())
			throw new PLCNotConnectedException();
		
		Logger.Debug("Trying to send a FINS message to the PLC at "+this.ip+":"+this.port);
		int[] response = null;
		
		try {
			Logger.Debug("Sending message to PLC at "+this.ip+":"+this.port);
			Logger.Debug(message.getMessageBytes());
			streamToServer.write(message.getMessageBytes());
		} catch (IOException e) {
			// TODO Launch exception
		}
		
		waitResponse();
		
		Logger.Debug("Waiting for response...");
		//get the response
        byte[] inputBytes = new byte[64];
        int bytesRead = -1;
        try {
        	//bytesRead = streamFromServer.read(inputBytes);
            if (streamFromServer.available() > 0) {
            	Logger.Debug("There is response from "+this.ip+":"+this.port);
                bytesRead = streamFromServer.read(inputBytes);
            }
        	
            if (bytesRead > 0) {
            	Logger.Debug("There is response from "+this.ip+":"+this.port);
            	byte[] response_bytes = new byte[bytesRead];
                response = new int[bytesRead];
                
                for (int i = 0; i < bytesRead; i++) {
                    response[i] = inputBytes[i];
                    response_bytes[i] = inputBytes[i];
                }
                Logger.Debug(response_bytes); 
            } else {
            	Logger.Debug("There is no response");
            }
        } catch (IOException e) {
        	//TODO throw exception
            e.printStackTrace();
        }
        
        return response;
	}
	
	/**
	 * Waits for a response from the PLC a maximum time of {@link Configuration#getMaxWaitTime()}.
	 * @throws NoPLCResponseException If there is no response from the PLC in the given time.
	 */
	private void waitResponse() throws NoPLCResponseException {
		Instant start = Instant.now();
		int availableBytes = 0;
		
		try {
			while((availableBytes = streamFromServer.available()) == 0 
					&& Duration.between(start, Instant.now()).toMillis() 
						< Configuration.getMaxWaitTime()) {
				try {
					Thread.sleep(Configuration.getSleepResponseTime());
				} catch (InterruptedException e) {
					// Do nothing
				}
			}
		} catch( IOException e) {
			throw new NoPLCResponseException();
		}

		if (availableBytes == 0) {
			// Close socket and exit
			try {
				plcConnection.close();
			} catch (IOException e) {
				// TODO ver que hacemos con esta excepcion
			}
			throw new NoPLCResponseException();
        }
	}
	
	public String toString() {
		return this.ip + ":" + this.port;
	}
}
