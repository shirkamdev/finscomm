package connection.exeptions;

public class PLCNotConnectedException extends Exception {
	private static final long serialVersionUID = -3077896856452256226L;

	public PLCNotConnectedException() {
		super("Error: You are not connected to any PLC.");
	}
}
