package connection.exeptions;

public class PLCConnectResponseErrorException extends Exception {
	private static final long serialVersionUID = -1044541868684867454L;
	
	public PLCConnectResponseErrorException(String ip, int port) {
		super("Error: Could not read response from PLC at "+ip+":"+port+
				". Check connectivity and PLC configuration and try again.");
	}
}
