package configuration;

import models.Variable;

/**
 * Contains information that allows configuration of the library. These methods should be used to
 * tweak library behaviour.
 */
public class Configuration {

	/**
	 * Time in milliseconds to sleep between checks when waiting for a response from a PLC
	 */
	private static long SLEEP_TIME_RESPONSE = 50;
	
	/**
	 * Show debug messages
	 */
	private static boolean DEBUG = false;
	
	/**
	 * If the library should auto-update variable values when needed.
	 * 
	 * This will be done when the {@link Variable#lastUpdate } elapsed time is bigger
	 * than {@link Configuration#AUTO_UPDATE_TIME AutoUpdateTime}.
	 */
	private static boolean AUTO_UPDATE_VAR_VALUES = true;
	
	/**
	 * Maximun response wait time in milliseconds
	 */
	private static long WAIT_MAX_TIME = 5000; 
	
	/**
	 * Time in milliseconds to wait until auto updating a variable
	 */
	private static long AUTO_UPDATE_TIME = 5000;
	
	/**
	 * Returns if the library is showing debug output.
	 * @return If the library is showing debug output.
	 */
	public static boolean isDebug() {
		return DEBUG;
	}
	
	/**
	 * Set if the library should show debug output.
	 * @param debug Show or not debug ouput.
	 */
	public static void setDebug(boolean debug) {
		DEBUG = debug;
	}
	
	/**
	 * Returns whether the {@link Variable } value is auto updating when the elapsed time is bigger
	 * than the threshold {@link Configuration#AUTO_UPDATE_TIME AutoUpdateTime} or not.
	 * @return If Variable value is auto-updated
	 */
	public static boolean isVarAutoUpdateActivated() {
		return AUTO_UPDATE_VAR_VALUES;
	}
	
	/**
	 * Sets whether the {@link Variable } value is auto updating when the elapsed time is bigger
	 * than the threshold {@link Configuration#AUTO_UPDATE_TIME AutoUpdateTime} or not.
	 * @param autoUpdateVarValues Set variable value to auto-update
	 */
	public static void setVarAutoUpdateActivated(boolean autoUpdateVarValues) {
		AUTO_UPDATE_VAR_VALUES = autoUpdateVarValues;
	}
	
	/**
	 * Gets time (in milliseconds) when to auto-update a variable
	 * @return The auto-update time in milliseconds
	 */
	public static long getVarAutoUpdateTime() {
		return AUTO_UPDATE_TIME;
	}
	
	/**
	 * Sets time (in milliseconds) when to auto-update a variable
	 * @param autoUpdateTime The auto-update time in milliseconds
	 */
	public static void setAUTO_UPDATE_TIME(long autoUpdateTime) {
		AUTO_UPDATE_TIME = autoUpdateTime;
	}
	
	/**
	 * Return the maximum time (in milliseconds) to wait a PLC response.
	 * @return The maximum time to wait (in ms) a PLC response.
	 */
	public static long getMaxWaitTime() {
		return WAIT_MAX_TIME;
	}
	
	/**
	 * Set the maximum time (in milliseconds) to wait a PLC response
	 * @param maxWaitTime Max time to wait (in ms) a PLC response.
	 */
	public static void setMaxWaitTime(long maxWaitTime) {
		WAIT_MAX_TIME = maxWaitTime;
	}
	
	/**
	 * Returns the time to wait (in milliseconds) between checks of if there is a response from a 
	 * PLC. 
	 * @return The time (in ms) to wait between every check of if there is a response from a PLC.
	 */
	public static long getSleepResponseTime() {
		return SLEEP_TIME_RESPONSE;
	}
	
	/**
	 * Sets the time to wait (in milliseconds) between checks of if there is a response from a 
	 * PLC. 
	 * @param sleepResponseTime The time (in ms) to wait between every check of if there is a 
	 * response from a PLC.
	 */
	public static void setSleepResponseTime(long sleepResponseTime) {
		SLEEP_TIME_RESPONSE = sleepResponseTime;
	}

}
