import configuration.Configuration;
import connection.exeptions.CouldNotAwaitForResponseException;
import connection.exeptions.NoPLCResponseException;
import connection.exeptions.PLCConnectErrorException;
import connection.exeptions.PLCConnectResponseErrorException;
import connection.exeptions.PLCNotConnectedException;
import connection.exeptions.PLCRefusesConnectionException;
import models.IECTypes;
import models.PLC;
import models.Variable;

public class MainClass {
	public static void main(String[] args) {
		/************************************
		 * CAMBIAR LAS VARIABLES IP Y PUERTO A LOS REALES 
		 * EL PUERTO POR DEFECTO ES EL 9600.
		 * **********************************/
		
		String IP = "212.170.116.137";
		int PUERTO = 23301;
		
		//////////////////////////
		// NO TOCAR ESTO
		
		Configuration.setDebug(true);
		
		//testMEM_AREA_READ(IP, PUERTO);
		testMULTIPLE_MEM_AREA_READ(IP, PUERTO);
	}
	
	private static void testMEM_AREA_READ(String IP, int PUERTO) {
		int MEM_AREA = 0x82;
		int MEM_ADRESS = 600;
				
		try {
			String varName = "Test";
			
			// Esto crea el PLC e intenta conectarse a su homonimo fisico
			PLC plcTest = new PLC("PLC de prueba", IP, PUERTO);
			plcTest.addVariable(MEM_AREA, MEM_ADRESS, IECTypes.INT, varName);
			System.out.println("El valor es: "+plcTest.getVariableValue(varName));
			plcTest.disconnect();
			
		} catch (PLCConnectErrorException 
				| NoPLCResponseException 
				| PLCConnectResponseErrorException
				| PLCRefusesConnectionException e) {
			e.printStackTrace();
		} catch (PLCNotConnectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void testMULTIPLE_MEM_AREA_READ(String IP, int PUERTO) {
		int MEM_AREA = 0x82;
		int MEM_ADRESS = 600;
		int MEM_ADRESS_2 = 602;
		
		Configuration.setVarAutoUpdateActivated(false);
		
		try {			
			// Esto crea el PLC e intenta conectarse a su homonimo fisico
			PLC plcTest = new PLC("PLC de prueba", IP, PUERTO);
			String name1 = "Var1";
			String name2 = "Var2";
			
			Variable var1 = plcTest.addVariable(MEM_AREA, MEM_ADRESS, IECTypes.INT);
			Variable var2 = plcTest.addVariable(MEM_AREA, MEM_ADRESS_2, IECTypes.INT);
			
			
			String[] names = new String[] {name1, name2};
			plcTest.getVariablesValues(names);
			
			System.out.println(var1.toString());
			System.out.println(var2.toString());
			
			plcTest.disconnect();
		} catch (PLCConnectErrorException  
				| PLCConnectResponseErrorException
				| PLCRefusesConnectionException 
				| NoPLCResponseException
				| CouldNotAwaitForResponseException e) {
			e.printStackTrace();
		} catch (PLCNotConnectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
