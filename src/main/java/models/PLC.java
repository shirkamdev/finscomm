package models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import configuration.Logger;
import connection.Connection;
import connection.exeptions.CouldNotAwaitForResponseException;
import connection.exeptions.NoPLCResponseException;
import connection.exeptions.PLCConnectErrorException;
import connection.exeptions.PLCConnectResponseErrorException;
import connection.exeptions.PLCNotConnectedException;
import connection.exeptions.PLCRefusesConnectionException;
import models.FINS.FINSCode;
import models.FINS.FINSMessage;

/**
 * Represents the connection with an external PLC with FINS. It handles all logic between the user 
 * and the library, allowing to add variables, read their values, and write them.
 *
 */
public class PLC {
	/**
	 * The PLC name. Now this has no significative importance.
	 */
	private String plcName;
	/**
	 * The {@link Connection} with the PLC.
	 */
	private Connection connection;
	
	/**
	 * Set of the variables stored in the PLC, ordered by address.
	 */
	private SortedSet<Variable> variables;
	/**
	 * Hash containing all variables stored in {@link PLC#variables varaibles set} to be able to
	 * obtain them by its name in O(1).
	 */
	private HashMap<String, Variable> variablesByName;
	/**
	 * Hash containing all variables stored in {@link PLC#variables varaibles set} to be able to
	 * obtain them by its address in O(1).
	 */
	private HashMap<Integer, Variable> variablesByAddress;
	
	/**
	 * Setups a PLC connection, given its IP an PORT.
	 * @param name The name of the PLC.
	 * @param ip The IP of the PLC we are gonna connect to.
	 * @param port The port used on the connection.
	 * @throws PLCConnectErrorException If there was an error on the connection.
	 * @throws NoPLCResponseException If the PLC does not respond in a time.
	 * @throws PLCConnectResponseErrorException If the PLC response has errors.
	 * @throws PLCRefusesConnectionException If the PLC refuses the connection.
	 */
	public PLC(String name, String ip, int port) 
			throws PLCConnectErrorException, NoPLCResponseException, 
			PLCConnectResponseErrorException, PLCRefusesConnectionException {
		this.plcName = name;
		this.connection = new Connection(ip, port);
		
		variables = new TreeSet<Variable>(Comparator.comparing(Variable::getMemoryAddress));
		variablesByName = new HashMap<>();
		variablesByAddress = new HashMap<>();
		
		// Conectar con el PLC real
		this.connection.connect();
	}
	
	public String getPLCName() {
		return this.plcName;
	}
	
	public void setPLCName(String plcName) {
		this.plcName = plcName;
	}
	
	/**
	 * Adds a variable to PLC memory map. It autogenerates a variable name and gives it to the new
	 * created variable, which is returned for future references.
	 * @param memArea The memory area identifier
	 * @param memAddress The memory address of the variable.
	 * @param type The type of the variable.
	 * @return The generated variable, for future references.
	 */
	public Variable addVariable(int memArea, int memAddress, IECTypes type) {
		// Auto generated name
		String varName = "Var"+(variables.size()+1);
		return addVariable(memArea, memAddress, type, varName);
	}
	
	/**
	 * Adds a variable to PLC memory maps. This variable will be accessible by
	 * its name and by its address.
	 * @param memArea The memory area identifier
	 * @param memAddress The memory address of the variable.
	 * @param type The type of the variable.
	 * @param name The name of the variable.
	 * @return The new created variable, for future references.
	 */
	public Variable addVariable(int memArea, int memAddress, IECTypes type, String name) {
		Variable var = new Variable(memArea, memAddress, type, name, this);
		
		variables.add(var);
		variablesByName.put(name, var);
		variablesByAddress.put(memAddress, var);
		
		return var;
	}
	
	/**
	 * Obtains a variable value by it's name.
	 * @see Variable#getValue() Variable.getValue() for reference about how this value is obtained.
	 * @param name The name of the variable.
	 * @return The value of the variable.
	 * @throws PLCNotConnectedException If the PLC is not connected.
	 */
	public String getVariableValue(String name) 
			throws PLCNotConnectedException {
		if(!this.connection.isConnected())
			throw new PLCNotConnectedException();
		
		if(!variablesByName.containsKey(name))
			throw new IllegalArgumentException("Error: There is no variable "
					+ "with name "+name);
		return variablesByName.get(name).getValue();
	}
	
	/**
	 * Obtains a variable value by it's address.
	 * @see Variable#getValue() Variable.getValue() for reference about how this value is obtained.
	 * @param address The address of the variable.
	 * @return The value of the variable.
	 * @throws PLCNotConnectedException If the PLC is not connected.
	 */
	public String getVariableValue(int direction) 
			throws PLCNotConnectedException {
		if(!this.connection.isConnected())
			throw new PLCNotConnectedException();
		
		if(!variablesByAddress.containsKey(direction))
			throw new IllegalArgumentException("Error: There is no variable "
					+ "in direction "+direction);
		return variablesByAddress.get(direction).getValue();
	}
	
	/**
	 * Gets the values of a list of variables. It generates a 
	 * {@link FINSCode#MULTIPLE_MEMORY_AREA_READ} operation with every variable. If any name is not
	 * registered by this object, it throws {@link IllegalArgumentException}.
	 * @param names The names of the variables.
	 * @return A list of values in the same order of the names given.
	 * @throws PLCNotConnectedException If the PLC is not connected.
	 * @throws NoPLCResponseException If there is no PLC response.
	 */
	public List<String> getVariablesValues(String... names) throws PLCNotConnectedException, 
	NoPLCResponseException {		
		if(!this.connection.isConnected())
			throw new PLCNotConnectedException();
		
		List<Variable> variables = new ArrayList<>();
		for(String name: names) {
			if(!variablesByName.containsKey(name))
				throw new IllegalArgumentException("Error: There is no variable "
						+ "with name "+name);
			
			variables.add(variablesByName.get(name));
		}
		
		return getVariablesValues(variables);
	}
	
	/**
	 * Gets the values of a list of variables. It generates a 
	 * {@link FINSCode#MULTIPLE_MEMORY_AREA_READ} operation with every variable. If any address is 
	 * not registered by this object, it throws {@link IllegalArgumentException}.
	 * @param addresses The addresses of the variables.
	 * @return A list of values in the same order of the addresses given.
	 * @throws PLCNotConnectedException If the PLC is not connected.
	 * @throws NoPLCResponseException If there is no PLC response.
	 */
	public List<String> getVariablesValues(Integer... addresses) throws PLCNotConnectedException, 
	CouldNotAwaitForResponseException, NoPLCResponseException {
		if(!this.connection.isConnected())
			throw new PLCNotConnectedException();
		
		List<Variable> variables = new ArrayList<>();
		for(Integer address: addresses) {
			if(!variablesByAddress.containsKey(address))
				throw new IllegalArgumentException("Error: There is no variable "
						+ "with address "+address);
			
			variables.add(variablesByAddress.get(address));
		}
		
		return getVariablesValues(variables);
	}
	
	/**
	 * Gets the values of a list of variables. It generates a 
	 * {@link FINSCode#MULTIPLE_MEMORY_AREA_READ} operation with every variable.
	 * @param varaibles The varaibles to read.
	 * @return A list of values in the same order of the addresses given.
	 * @throws PLCNotConnectedException If the PLC is not connected.
	 * @throws NoPLCResponseException If there is no PLC response.
	 */
	private List<String> getVariablesValues(List<Variable> variables) 
			throws NoPLCResponseException {
		List<String> values = new ArrayList<>();
		FINSMessage message = new FINSMessage(FINSCode.MULTIPLE_MEMORY_AREA_READ, variables);
		try {
			int[] result = this.connection.sendMessageToPLC(message);
			if(result == null)
				throw new NoPLCResponseException();
			
			// TODO Change variables to recieve MEM AREA from Enum (to have a fixed size).
			int start_index = result.length - variables.size() * 3;
			int varIndex = 0;
			
			for(int i = start_index; i<result.length; i+=3, varIndex++) {
				int upperByte = result[i+1];
				int lowerByte = result[i+2];
				
				String val = calculateDMVal(upperByte, lowerByte)+"";
				values.add(val);
				variables.get(varIndex).setValueInternal(val);
			}
			
		} catch (PLCNotConnectedException e) {
			// This will not happen
		} catch (PLCConnectErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PLCConnectResponseErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PLCRefusesConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return values;
	}
	
	/**
	 * Reads from the PLC (via FINS) the value of a variable and stores it.
	 * @param var
	 */
	public void readVariableValueFromPLC(Variable var) {
		Logger.Debug("Reading value for vaeiable "+var);
		FINSMessage message = new FINSMessage(FINSCode.MEMORY_AREA_READ, var);
		int response[] = null;
		try {
			response = this.connection.sendMessageToPLC(message);
		} catch (PLCNotConnectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PLCConnectErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PLCConnectResponseErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PLCRefusesConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(NoPLCResponseException e) {
			// TODO Do this
			e.printStackTrace();
		}
		
		if (response != null) {
            int upperByte = response[response.length-2];
            int lowerByte = response[response.length-2];
            
            int res = calculateDMVal(upperByte, lowerByte);
            Logger.Debug("The variable value is "+res);
            
            var.setValueInternal(res+"");
        } else {
        	var.setValueInternal("0");
        }
	}
	
	/**
	 * Calculates integer value from two bytes
	 * @param upperByte The upper byte
	 * @param lowerByte The lower byte
	 * @return The integer value from those two bytes, using the formula: <br/>
	 * result = upperByte * 256 + lowerByte
	 */
	public int calculateDMVal(int upperByte, int lowerByte) {
		if (lowerByte < 0) {
            lowerByte += 256;
        }
        
        return upperByte * 256 + lowerByte;
	}
	
	/**
	 * Disconnects from PLC.
	 */
	public void disconnect() {
		// Launch exception if other operation is launched after this
		this.connection.disconnect();
		Logger.Debug("Disconnected from "+this.connection);
	}
}
