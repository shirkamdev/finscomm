package models;

/**
 * Representa todos los tipos de variables definidas en el IEC asi como su
 * tamanyo en bytes.
 *
 */
public enum IECTypes {
	/////////////
	// Integers
	/////////////
	/**
	 * Bit stream, uses 1 byte.
	 */
	BYTE(1), // 8 bits
	/**
	 * Bit stream, uses 2 bytes.
	 */
	WORD(2), // 16 bits
	/**
	 * Bit stream, uses 4 bytes.
	 */
	DWORD(4), // 32 bits
	// Integers
	/**
	 * Signed short integer, uses 1 byte.
	 */
	SINT(1), // 8 bits, signed short integer
	/**
	 * Signed integer, uses 2 bytes
	 */
	INT(2), // 16 bits, signed integer
	/**
	 * Signed double integer, uses 4 bytes.
	 */
	DINT(4), // 32 bits, signed double integer
	/**
	 * Signed large integer, uses 8 bytes.
	 */
	LINT(8), // 64 bits, signed large integer
	/**
	 * Unsigned short integer, uses 1 byte.
	 */
	USINT(1), // 8 bits, unsigned short integer
	/**
	 * Unsigned integer, uses 2 bytes.
	 */
	UINT(2), // 16 bits, unsigned integer
	/**
	 * Unsigned double integer, uses 4 bytes
	 */
	UDINT(4), // 32 bits, unsigned double integer
	/**
	 * Unsigned large integer, uses 8 bytes
	 */
	ULINT(8), //64 bits, unsigned large integer
	
	/////////////
	// Reals
	/////////////
	/**
	 * Floating point number, uses 4 bytes.
	 */
	REAL(4), // 32 bits
	/**
	 * Large floating point number, uses 8 bytes.
	 */
	LREAL(8), // 64 bits
	// Duration
	/**
	 * Time, uses 4 bytes. It goes in format T#AmBsCms. It goes up to ms of
	 * precission. You can go up as you wish.
	 */
	TIME(4), // Time, in format T#5m10s45ms
	
	/////////////
	// Chars
	/////////////
	
	/**
	 * Represents a single character. Uses 1 byte.
	 */
	CHAR(1), // 8 bits, character
	/**
	 * Represents an string of characters, uses as much bytes as characters it
	 * has inside.
	 */
	STRING(0); // From 1 to x characters, user must specify length
	
	/**
	 * Size in bytes of a type.
	 */
	public final short size;
	
	private IECTypes(int size) {
		this.size = (short) size;
	}
}
