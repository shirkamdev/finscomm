# FINSComm
## FINS communication library in JAVA

This library aims to provide an interface to communicate with an OMRON PLC using the FINS protocol.
By using this library, the user can abstract from having to perform all communication with the PLC. Instead of that, it should use the `PLC` class, that will have every basic operation needed.

An example program to show the easyness of this library would be:
```
String IP = "192.168.0.120";
int port = 9600;

int MEM_AREA = 0x82;
int MEM_ADRESS = 100;
String name = "Var1";
				
try {
	// This creates the PLC Object and tries to connect to the actual PLC
	PLC plcTest = new PLC("PLC de prueba", IP, port);
	
	// Add a variable to the PLC, these variables are used to get and set values.
	// If you don't asign a variable name, it calculates one for you.
	Variable var = plcTest.addVariable(MEM_AREA, MEM_ADRESS, IECTypes.INT, name);
	
	// Get variable value
	System.out.println("The value is: " + var.getValue());
	
	// You can also get a variable from it's name or it's address
	Variable var1 = plcTest.getVariableByName(name);
	Variable var2 = plcTest.getVariableByAddress(MEM_ADDRESS);
	
	// Disconnect from PLC
	plcTest.disconnect();
	
} catch (PLCConnectErrorException 
		| NoPLCResponseException 
		| PLCConnectResponseErrorException
		| PLCRefusesConnectionException
		| CouldNotAwaitForResponseException e) {
	e.printStackTrace();
} catch (PLCNotConnectedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
```

This example shows how to read the value of one variable. To see more operations, please, see documentation.

### Documentation
The main documents used to design this library where:

- SYSMAC CS and CJ Series CS1W-ETN21 CJ1W-ETN21 Ethernet Units: Construction of Applications Operation Manual
- SYSMAC CS/CJ/CP Series and SYSMAC One NSJ Series Communications Commands Reference Manual